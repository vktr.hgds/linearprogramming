This is a simplex algorithm solver written in Java language.



So far the simplex primal algorithm has been implemented yet.
How to use the program?
a,  Enter the matrix/simplex tableau values
	You can enter the values in the following format (this is an example):
	
	8 8 3
	1 5 1
	3 6 
	
	The application then creates the following tableau:
	
	    x1  x2  b
	y1  8   8   3
	y2  1   5   1
	-------------
    Z   3   6   0

	After this, you are able to run the algorithm, or to change a number's value, or a variable's name.
	
b, read the values from file
	After reading the values from the inputMatrix.txt, the program creates the simplex tableau from it.
	After all, you have an option to change a number's value, or a variable's name, or just run the algorithm.
	Finally, when the execution of the algorithm stops, the program writes the solution to a txt file called resultMatrix.txt.
