package com.hegedusviktor;

import java.io.FileWriter;
import java.io.IOException;

public class SimplexMaxPrimal<Type> extends Matrix {

    private Type[][] primalMatrix;
    public SimplexMaxPrimal(int rows, int columns) {
        super(rows, columns);
    }

    /**
     * helper function to find the generator element
     * CLASSIC PIVOT RULE -> MAXIMIZE
     * @return -> the maximum value in the last row
     */

    @SuppressWarnings("unchecked")
    public double findMaximumValue () {
        double maxVal = Integer.MIN_VALUE;
        primalMatrix = (Type[][]) super.getMatrix();

        for (int i = primalMatrix.length - 1; i < primalMatrix.length; i++) {
            for (int j = 1; j < primalMatrix[i].length - 1; j++) {
                if ((Double) primalMatrix[i][j] >= maxVal) {
                    maxVal = (Double) primalMatrix[i][j];
                }
            }
        }
        return maxVal;
    }

    /**
     * helper function to find the generator element
     * CLASSIC PIVOT RULE -> MAXIMIZE
     * @return -> the maximum value's column number
     */

    @SuppressWarnings("unchecked")
    public int findMaximumValueColumnNumber () {
        double maxVal = Integer.MIN_VALUE;
        int columnIndex = 0;
        primalMatrix = (Type[][]) super.getMatrix();

        for (int i = primalMatrix.length - 1; i < primalMatrix.length; i++) {
            for (int j = 1; j < primalMatrix[i].length - 1; j++) {
                if ((Double) primalMatrix[i][j] >= maxVal) {
                    maxVal = (Double) primalMatrix[i][j];
                    columnIndex = j;
                }
            }
        }
        return columnIndex;
    }

    /**
     * @return the variable name in the maximum value's column
     */

    public String findMaximumValueVariable () {
        primalMatrix = (Type[][]) super.getMatrix();
        return (String) primalMatrix[0][findMaximumValueColumnNumber()];
    }

    /**
     * @return the minimal element value, which is found in the maximum value's column
     */

    public double findMinimumValue() {

        primalMatrix = (Type[][]) super.getMatrix();
        double minVal = Integer.MAX_VALUE;
        int columnIndex = findMaximumValueColumnNumber();
        double exactElement = Integer.MAX_VALUE;

        for (int i = 1; i < primalMatrix.length - 1; i++) {
            for (int j = 1; j < primalMatrix[i].length; j++) {
                if ((Double) primalMatrix[i][primalMatrix[i].length - 1] >= 0 && (Double) primalMatrix[i][columnIndex] > 0) {

                   double currentMinVal = 0;
                   try {
                       currentMinVal = (Double) primalMatrix[i][primalMatrix[i].length - 1] / (Double) primalMatrix[i][columnIndex];
                   } catch (ArithmeticException e) {
                       e.printStackTrace();
                       System.out.println("Error! Division by 0");
                   } finally {
                       if (minVal > currentMinVal) {
                           minVal = currentMinVal;
                           exactElement = (Double) primalMatrix[i][columnIndex];
                       }
                   }
                }
            }
        }
        return exactElement;
    }

    /**
     * @return the minimal element' row index, which is found in the maximum value's column
     */

    public int findMinimumValueRowNumber() {

        primalMatrix = (Type[][]) super.getMatrix();
        double minVal = Integer.MAX_VALUE;
        int rowIndex = 0;
        int columnIndex = findMaximumValueColumnNumber();

        for (int i = 1; i < primalMatrix.length - 1; i++) {
            for (int j = 1; j < primalMatrix[i].length; j++) {
                if ((Double) primalMatrix[i][primalMatrix[i].length - 1] >= 0 && (Double) primalMatrix[i][columnIndex] > 0) {

                    double currentMinVal = 0;
                    try {
                        currentMinVal = (Double) primalMatrix[i][primalMatrix[i].length - 1] / (Double) primalMatrix[i][columnIndex];
                    } catch (ArithmeticException e) {
                        e.printStackTrace();
                        System.out.println("Error! Division by 0");
                    } finally {
                        if (minVal > currentMinVal) {
                            minVal = currentMinVal;
                            rowIndex = i;
                        }
                    }
                }
            }
        }
        return rowIndex;
    }

    /**
     * @return the variable name in the minimum value's row
     */

    @SuppressWarnings("unchecked")
    public String findMinimumValueVariable() {
        primalMatrix = (Type[][]) super.getMatrix();
        return (String) primalMatrix[findMinimumValueRowNumber()][0];
    }

    /**
     * swap the x & y variables
     * (in Hungarian it's called "bemeno" & "kimeno" variables)
     * @return
     */

    @SuppressWarnings("unchecked")
    public Type[][] swapVariables () {
        primalMatrix = (Type[][]) super.getMatrix();
        String tmp = (String) primalMatrix[0][findMaximumValueColumnNumber()];
        primalMatrix[0][findMaximumValueColumnNumber()] = primalMatrix[findMinimumValueRowNumber()][0];
        primalMatrix[findMinimumValueRowNumber()][0] = (Type) new String(tmp);
        return primalMatrix;
    }

    /**
     * run the simplex algorithm in the "classic" way
     * 1: swap the "kimeno" and "bemeno" variables
     * 2: change the value of the elements in the correct way
     *
     *       actual element: matrix[row][column]
     *
     *      - if the actual element's row & column index is not equal to
     *        the minimum value's row & column index, then the formula is the following:
     *        matrix[i][j] = matrix[i][j] - (matrix[i][columnIndex] / matrix[row][column]) * matrix[row][j];
     *      - if the actual element's rowindex is equal to min's element row index, but cols dont match:
     *          matrix[i][j] = matrix[i][j] / matrix[row][column];
     *
     *      - if the actual element's columnindex is equal to min's element col index, but rows dont match:
     *           matrix[i][j] = (-1) * (matrix[i][j] / matrix[row][column]);
     *
     *      - if it's the actual element, then:
     *          matrix[row][column] = 1 / matrix[row][column];
     *
     * @return: the matrix after an iteration
     */

    @SuppressWarnings("unchecked")
    public Type[][] runSimplex () {

        primalMatrix = (Type[][]) super.getMatrix();
        primalMatrix = swapVariables();

        int rowIndex = findMinimumValueRowNumber();
        int columnIndex = findMaximumValueColumnNumber();

        for (int i = 1; i < primalMatrix.length; i++) {
            for (int j = 1; j < primalMatrix[i].length; j++) {
                if (i != rowIndex && j != columnIndex) {
                    primalMatrix[i][j] = (Type) new Double((Double) primalMatrix[i][j]
                            - ((Double) primalMatrix[rowIndex][j] / (Double) primalMatrix[rowIndex][columnIndex])
                            * (Double) primalMatrix[i][columnIndex]);
                }
            }
        }

        for (int i = 1; i < primalMatrix.length; i++) {
            for (int j = 1; j < primalMatrix[i].length; j++) {
                if (i == rowIndex && j != columnIndex) {
                    primalMatrix[i][j] = (Type) new Double(((Double) primalMatrix[i][j] / (Double) primalMatrix[rowIndex][columnIndex]));
                } else if (i != rowIndex && j == columnIndex) {
                    primalMatrix[i][j] = (Type) new Double(-1 * ((Double) primalMatrix[i][j] / (Double) primalMatrix[rowIndex][columnIndex]));
                }
            }
        }


        primalMatrix[rowIndex][columnIndex] = (Type) new Double((1 / (Double) primalMatrix[rowIndex][columnIndex]));
        return primalMatrix;
    }

    /**
     * Check if the maximize problem has reached the optimal solution or not
     * @return true if the simplex tableau has optimal solution
     */

    public boolean isMaximizeOptimal () {

        boolean optimalSolution;
        if (findMaximumValue() <= 0) {
            optimalSolution = true;
        } else {
            optimalSolution = false;
        }
        return optimalSolution;
    }

    /**
     * Check if the linear programming task is bounded or not
     * @return
     */

    @SuppressWarnings("unchecked")
    public boolean hasBound () {

        primalMatrix = (Type[][]) super.getMatrix();
        int columnIndex = findMaximumValueColumnNumber();
        boolean hasBound = true;
        double maxValue = Integer.MIN_VALUE;

        for (int i = 1; i < primalMatrix.length - 1; i++) {
            for (int j = 1; j < primalMatrix[i].length; j++) {
                if ((Double) primalMatrix[i][columnIndex] >= maxValue) {
                    maxValue = (Double) primalMatrix[i][columnIndex];
                }
            }
        }

        if (maxValue <= 0 && !isMaximizeOptimal()) {
            hasBound = false;
        }

        return hasBound;
    }

    /**
     * the values of the x & y variables
     */

    @SuppressWarnings("unchecked")
    public void finalSolution () {

        primalMatrix = (Type[][]) super.getMatrix();

        for (int i = 0; i < primalMatrix.length; i++) {
            for (int j = 0; j < primalMatrix[i].length; j++) {

                if (i == 0 && j != 0 && j != (primalMatrix[i].length - 1)) {
                    String variableName = (String) primalMatrix[i][j];
                    double variableValue = new Double((Double) primalMatrix[primalMatrix.length - 1][j]);
                    System.out.println(variableName + " = " + variableValue);
                } else if (j == 0 && i != 0) {
                    String variableName = (String) primalMatrix[i][j];
                    double variableValue = new Double((Double) primalMatrix[i][primalMatrix[i].length - 1]);
                    System.out.println(variableName + " = " + variableValue);
                }
            }
        }
    }

    /**
     * Write the 2d array to file
     * @throws IOException
     */


    @SuppressWarnings("unchecked")
    public void writeToFile () throws IOException {

        primalMatrix = (Type[][]) super.getMatrix();
        double[][] saveMatrix = new double[primalMatrix.length - 1][primalMatrix[0].length - 1];

        for (int i = 0; i < primalMatrix.length - 1; i++) {
            for (int j = 0; j < primalMatrix[i].length - 1; j++) {
                saveMatrix[i][j] = (Double) primalMatrix[i + 1][j + 1];
            }
        }

        String fileName = "resultMatrix.txt";
        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(fileName);
            for (int i = 0; i < saveMatrix.length; i++) {
                for (int j = 0; j < saveMatrix[i].length; j++) {
                    fileWriter.write(saveMatrix[i][j] + (j == saveMatrix[i].length - 1 ? "" : " "));
                }
                fileWriter.write("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileWriter.close();
        }
    }
}
