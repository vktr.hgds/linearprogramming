package com.hegedusviktor;

public class SimplexMinPrimal<Type extends Object> extends Matrix  {

    private Type[][] primalMatrix;

    public SimplexMinPrimal(int rows, int columns) {
        super(rows, columns);
    }

    /**
     * helper function to find the generator element
     * CLASSIC PIVOT RULE -> MINIMIZE
     */

    @SuppressWarnings("unchecked")
    public double findMinimumValue () {
        double minVal = Integer.MAX_VALUE;
        primalMatrix = (Type[][]) super.getMatrix();

        for (int i = primalMatrix.length - 1; i < primalMatrix.length; i++) {
            for (int j = 1; j < primalMatrix[i].length - 1; j++) {
                if ((Double) primalMatrix[i][j] <= minVal) {
                    minVal = (Double) primalMatrix[i][j];
                }
            }
        }
        return minVal;
    }


    /**
     * Check if the minimize problem has reached the optimal solution or not
     * @return true if the simplex tableau has optimal solution
     */

    public boolean isMinimizeOptimal () {
        boolean optimalMinimizeSolution;
        if (findMinimumValue() >= 0) {
            optimalMinimizeSolution = true;
        } else {
            optimalMinimizeSolution = false;
        }
        return optimalMinimizeSolution;
    }

    /**
     * helper function to find the generator element
     * BLAND PIVOT RULE -> MINIMIZE
     */

    @SuppressWarnings("unchecked")
    public double findFirstNegativeValue () {

        double minVal = Integer.MAX_VALUE;
        primalMatrix = (Type[][]) super.getMatrix();

        searchFirstPositive:
        for (int i = primalMatrix.length - 1; i < primalMatrix.length; i++) {
            for (int j = 1; j < primalMatrix[i].length - 1; j++) {
                if ((Double) primalMatrix[i][j] <= minVal && (Double) primalMatrix[i][j] < 0) {
                    minVal = (Double) primalMatrix[i][j];
                    break searchFirstPositive;
                }
            }
        }
        return minVal;
    }

}
