package com.hegedusviktor;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static Scanner scanner2 = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        runProgram();
    }

    /**
     * Print linear programming application's options the console
     */

    private static void mainMenu () {
        System.out.println("Linear Programming algorithms\n");
        System.out.println("(1) Primal Simplex");
        System.out.println("(2) Two-Phase Simplex");
        System.out.println("(3) Dual Simplex");
        System.out.println("(4) Dual All Integer");
        System.out.println("(5) Show menu");
        System.out.println("(6) Help");
        System.out.println("(7) Quit");
        System.out.println();
    }

    /**
     * Print the primal simplex algorithm's options to console
     */


    private static void primalMainMenu() {
        System.out.println("\n(1) Read values from keyboard");
        System.out.println("(2) Read values from inputMatrix.txt file");
        System.out.println("(3) Generate random values");
        System.out.println("(4) Quit");
    }


    /**
     * This method helps the user to correctly use the application
     */

    private static void helpUser () {
        System.out.println("\nLinear programming algorithms\n");
        System.out.println("You can use the application from the keyboard, or reading the values from a txt file.");
        System.out.println("The valid name of the txt file you read the values from is inputMatrix.txt");
        System.out.println("If you don't find this file, then create one with the correct name");
        System.out.println("and add some value to it in the following format (this is an example):");
        System.out.println("8 8 64\n" +
                "1 3 15\n" +
                "3 0 18\n" +
                "0 2 8\n" +
                "5 3 0\n");
    }

    /**
     * This method is for the unfinished implementations
     */

    private static void notDoneYet () {
        System.out.println("\nAlgorithm has not been implemented yet.");
    }

    /**
     * Message to console - solution
     */

    private static void foundSolutionMessage() {
        System.out.println("\n\n*********************************************");
        System.out.println("************** OPTIMAL SOLUTION *************");
        System.out.println("*********************************************\n");
    }

    /**
     * Message to console - no solution
     */

    private static void noBoundMessage() {
        System.out.println("\n\n*********************************************");
        System.out.println("********** NO SOLUTION - UNBOUNDED **********");
        System.out.println("*********************************************");
    }

    /**
     * Primal simplex algorithm's menu
     */

    private static void choosePrimal () {
        boolean exitPrimal = false;
        primalMainMenu();

        while (!exitPrimal) {
            System.out.print("\nEnter a value: ");
            int selectOption;
            if (scanner.hasNextInt()) {
                selectOption = scanner.nextInt();
                scanner.nextLine();

                switch (selectOption) {
                    case 1:
                        primalFromKeyboard();
                        primalMainMenu();
                        break;
                    case 2:
                        primalFromFile();
                        primalMainMenu();
                        break;
                    case 3:
                        generateSimplexTableau();
                        primalMainMenu();
                        break;
                    case 4:
                        mainMenu();
                        exitPrimal = true;
                        break;
                    default:
                        System.out.println("Enter a valid number!");
                        break;
                }
            }
        }
    }

    /**
     *  run primal simplex algorithm - input is accepted from the keyboard by the user
     */
    private static void generateSimplexTableau () {

        try {
            System.out.print("Enter the number of rows: ");
            int rows = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Enter the number of columns: ");
            int cols = scanner.nextInt();
            scanner.nextLine();

            SimplexMaxPrimal<Object> generatePrimalMatrix = new SimplexMaxPrimal<>(rows + 1, cols + 1);
            generatePrimalMatrix.generateTableau();
            generatePrimalMatrix.printMatrix("Auto-generated simplex tableau");

            boolean changeValue = true;
            while (changeValue) {

                System.out.println("\nWould you like to change a number's value\nor a variable's name?");
                System.out.println("(1) Yes");
                System.out.println("(2) No");
                System.out.print("\nEnter a value: ");

                int selectOption;
                if (scanner.hasNextInt()) {
                    selectOption = scanner.nextInt();
                    scanner.nextLine();

                    switch (selectOption) {
                        case 1:

                            System.out.print("Enter a row number: ");
                            int rowNum = scanner.nextInt();
                            scanner.nextLine();
                            System.out.print("Enter a column number: ");
                            int colNum = scanner.nextInt();
                            scanner.nextLine();

                            if (rowNum == 0 || colNum == 0) {
                                System.out.print("Enter new variable name: ");
                                String newName = scanner.nextLine();
                                generatePrimalMatrix.changeValue(rowNum, colNum, newName);
                                try {
                                    generatePrimalMatrix.printMatrix("Variable's name has been successfully modified\n");
                                } catch (Exception e) {
                                    System.out.println("Error! Cannot print the simplex tableau to the console.");
                                }
                            } else {
                                System.out.print("Enter new value: ");
                                double newValue = scanner.nextDouble();
                                scanner.nextLine();
                                generatePrimalMatrix.changeValue(rowNum, colNum, newValue);
                                try {
                                    generatePrimalMatrix.printMatrix("Number's value has been successfully modified\n");
                                } catch (Exception e) {
                                    System.out.println("Error! Cannot print the simplex tableau to the console.");
                                }
                            }

                            break;

                        case 2:
                            System.out.println("\nAlgorithm is running...");
                            try {
                                generatePrimalMatrix.printMatrix("Simplex Tableau");
                            } catch (Exception e) {
                                System.out.println("Error! Cannot print the simplex tableau to the console.");
                            }

                            changeValue = false;
                            break;
                        default:
                            System.out.println("\nInvalid value. Enter another number!\n");
                            break;
                    }
                }
            }

            int iteration = 0;

            if (generatePrimalMatrix.isMaximizeOptimal()) {
                foundSolutionMessage();
                generatePrimalMatrix.finalSolution();
            } else if (!generatePrimalMatrix.hasBound()) {
                noBoundMessage();
            } else {
                while (!(generatePrimalMatrix.isMaximizeOptimal()) && generatePrimalMatrix.hasBound()) {

                    iteration++;

                    System.out.println("\n\n*********************************************");
                    System.out.println("**************** ITERATION " + iteration + " ****************");
                    System.out.println("*********************************************");

                    System.out.println("\nMaximum value: " + generatePrimalMatrix.findMaximumValue());
                    System.out.print("Maximum value's column index: " + generatePrimalMatrix.findMaximumValueColumnNumber());
                    System.out.println(" [" + generatePrimalMatrix.findMaximumValueVariable() + "]");
                    System.out.println("Minimum value: " + generatePrimalMatrix.findMinimumValue());
                    System.out.print("Minimum value's row index: " + generatePrimalMatrix.findMinimumValueRowNumber());
                    System.out.println(" [" + generatePrimalMatrix.findMinimumValueVariable() + "]\n");
                    generatePrimalMatrix.runSimplex();

                    try {
                        generatePrimalMatrix.printMatrix("New Tableau");
                    } catch (Exception e) {
                        System.out.println("Error! Cannot print the simplex tableau to the console.");
                    }
                }

                if (generatePrimalMatrix.isMaximizeOptimal()) {
                    foundSolutionMessage();
                    generatePrimalMatrix.finalSolution();
                    System.out.println("\niterations: " + iteration);
                    System.out.println("\nWould you like to save the solution to resultMatrix.txt?");
                    System.out.println("(1) Yes");
                    System.out.println("(2) No");

                    boolean validInt = false;
                    while (!validInt) {
                        System.out.print("Enter value: ");

                        int save = scanner.nextInt();
                        scanner.nextLine();

                        switch (save) {
                            case 1:
                                try {
                                    generatePrimalMatrix.writeToFile();
                                    System.out.println("\nSolution has been saved successfully!");
                                } catch (Exception e) {
                                    System.out.println("Couldn't write to file.");
                                }
                                validInt = true;
                                break;
                            case 2:
                                validInt = true;
                                break;
                            default:
                                System.out.println("Invalid value!");
                                break;
                        }
                    }


                } else if (!generatePrimalMatrix.hasBound()) {
                    noBoundMessage();
                }
            }

        } catch (Exception e) {
            System.out.println("Error, couldn't generate matrix.");
        }
    }

    /**
     * Simplex primal algorithm. This method uses the values that the user has given
     */


    private static void primalFromKeyboard () {
        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Enter the number of columns: ");
        int cols = scanner.nextInt();
        scanner.nextLine();

        SimplexMaxPrimal<Object> primalMatrix = new SimplexMaxPrimal<>(rows + 1, cols + 1);
        primalMatrix.createMatrixFromInput();

        try {
            primalMatrix.printMatrix("The following tableau has been created\n");
        } catch (Exception e) {
            System.out.println("Error! Cannot print the simplex tableau to the console.");
        }

        boolean changeValue = true;
        while (changeValue) {

            System.out.println("\nWould you like to change a number's value\nor a variable's name?");
            System.out.println("(1) Yes");
            System.out.println("(2) No");
            System.out.print("\nEnter a value: ");

            int selectOption;
            if (scanner.hasNextInt()) {
                selectOption = scanner.nextInt();
                scanner.nextLine();

                switch (selectOption) {
                    case 1:

                        System.out.print("Enter a row number: ");
                        int rowNum = scanner.nextInt();
                        scanner.nextLine();
                        System.out.print("Enter a column number: ");
                        int colNum = scanner.nextInt();
                        scanner.nextLine();

                        if (rowNum == 0 || colNum == 0) {
                            System.out.print("Enter new variable name: ");
                            String newName = scanner.nextLine();
                            primalMatrix.changeValue(rowNum, colNum, newName);
                            try {
                                primalMatrix.printMatrix("Variable's name has been successfully modified\n");
                            } catch (Exception e) {
                                System.out.println("Error! Cannot print the simplex tableau to the console.");
                            }
                        } else {
                            System.out.print("Enter new value: ");
                            double newValue = scanner.nextDouble();
                            scanner.nextLine();
                            primalMatrix.changeValue(rowNum, colNum, newValue);
                            try {
                                primalMatrix.printMatrix("Number's value has been successfully modified\n");
                            } catch (Exception e) {
                                System.out.println("Error! Cannot print the simplex tableau to the console.");
                            }
                        }

                        break;

                    case 2:
                        System.out.println("\nAlgorithm is running...");
                        try {
                            primalMatrix.printMatrix("Simplex Tableau");
                        } catch (Exception e) {
                            System.out.println("Error! Cannot print the simplex tableau to the console.");
                        }

                        changeValue = false;
                        break;
                    default:
                        System.out.println("\nInvalid value. Enter another number!\n");
                        break;
                }
            }
        }

        int iteration = 0;

        if (primalMatrix.isMaximizeOptimal()) {
            foundSolutionMessage();
            primalMatrix.finalSolution();
        } else if (!primalMatrix.hasBound()) {
            noBoundMessage();
        } else {
            while (!(primalMatrix.isMaximizeOptimal()) && primalMatrix.hasBound()) {

                iteration++;

                System.out.println("\n\n*********************************************");
                System.out.println("**************** ITERATION " + iteration + " ****************");
                System.out.println("*********************************************");

                System.out.println("\nMaximum value: " + primalMatrix.findMaximumValue());
                System.out.print("Maximum value's column index: " + primalMatrix.findMaximumValueColumnNumber());
                System.out.println(" [" + primalMatrix.findMaximumValueVariable() + "]");
                System.out.println("Minimum value: " + primalMatrix.findMinimumValue());
                System.out.print("Minimum value's row index: " + primalMatrix.findMinimumValueRowNumber());
                System.out.println(" [" + primalMatrix.findMinimumValueVariable() + "]\n");
                primalMatrix.runSimplex();

                try {
                    primalMatrix.printMatrix("New Tableau");
                } catch (Exception e) {
                    System.out.println("Error! Cannot print the simplex tableau to the console.");
                }
            }

            if (primalMatrix.isMaximizeOptimal()) {
                foundSolutionMessage();
                primalMatrix.finalSolution();
                System.out.println("\niterations: " + iteration);
                System.out.println("\nWould you like to save the solution to resultMatrix.txt?");
                System.out.println("(1) Yes");
                System.out.println("(2) No");

                boolean validInt = false;
                while (!validInt) {
                    System.out.print("Enter value: ");

                    int save = scanner.nextInt();
                    scanner.nextLine();

                    switch (save) {
                        case 1:
                            try {
                                primalMatrix.writeToFile();
                                System.out.println("\nSolution has been saved successfully!");
                            } catch (Exception e) {
                                System.out.println("Couldn't write to file.");
                            }
                            validInt = true;
                            break;
                        case 2:
                            validInt = true;
                            break;
                        default:
                            System.out.println("Invalid value!");
                            break;
                    }
                }


            } else if (!primalMatrix.hasBound()) {
                noBoundMessage();
            }
        }
    }

    /**
     * Linear programming application's main menu
     */

    private static void runProgram () {
        boolean exitProgram = false;
        mainMenu();

        while (!exitProgram) {

            System.out.print("Enter a value: ");
            int selectOption;

            if (scanner.hasNextInt()) {
                selectOption = scanner.nextInt();
                scanner.nextLine();

                switch (selectOption) {
                    case 1:
                        choosePrimal();
                        break;
                    case 2:
                        notDoneYet();
                        break;
                    case 3:
                        notDoneYet();
                        break;
                    case 4:
                        notDoneYet();
                        break;
                    case 5:
                        mainMenu();
                        break;
                    case 6:
                        helpUser();
                        break;
                    case 7:
                        System.out.println("\nThe program has stopped.\nQuitting...");
                        exitProgram = true;
                        break;
                    default:
                        System.out.println("\nInvalid value. Enter another number!\n");
                        break;
                }
            } else {
                throw new InputMismatchException("Not a number");
            }
        }
    }

    /**
     * Read matrix from file, run the algorithm, then write the solution the resultMatrix.txt
     */


    private static void primalFromFile () {

        int rows, columns;
        try {
            rows = Matrix.numberOfRows() + 1;
            columns = Matrix.numberOfColumns() + 1;
            SimplexMaxPrimal<Object> primalMatrixFile = new SimplexMaxPrimal<>(rows, columns);
            primalMatrixFile.createMatrixFromFile();
            primalMatrixFile.printMatrix("The following tableau has been read from inputMatrix.txt file");

            boolean changeValue = true;
            while (changeValue) {

                System.out.println("\nWould you like to change a number's value\nor a variable's name?");
                System.out.println("(1) Yes");
                System.out.println("(2) No");
                System.out.print("\nEnter a value: ");

                int selectOption;
                if (scanner.hasNextInt()) {
                    selectOption = scanner.nextInt();
                    scanner.nextLine();

                    switch (selectOption) {
                        case 1:

                            System.out.print("Enter a row number: ");
                            int rowNum = scanner.nextInt();
                            scanner.nextLine();
                            System.out.print("Enter a column number: ");
                            int colNum = scanner.nextInt();
                            scanner.nextLine();

                            if (rowNum == 0 || colNum == 0) {
                                System.out.print("Enter new variable name: ");
                                String newName = scanner.nextLine();
                                primalMatrixFile.changeValue(rowNum, colNum, newName);
                                primalMatrixFile.printMatrix("Variable's name has been successfully modified\n");
                            } else {
                                System.out.print("Enter new value: ");
                                double newValue = scanner.nextDouble();
                                scanner.nextLine();
                                primalMatrixFile.changeValue(rowNum, colNum, newValue);
                                primalMatrixFile.printMatrix("Number's value has been successfully modified\n");

                            }

                            break;

                        case 2:
                            System.out.println("\nAlgorithm is running...");
                            primalMatrixFile.printMatrix("Simplex Tableau");
                            changeValue = false;
                            break;
                        default:
                            System.out.println("\nInvalid value. Enter another number!\n");
                            break;
                    }
                }
            }

            int iteration = 0;

            if (primalMatrixFile.isMaximizeOptimal()) {
               foundSolutionMessage();
                primalMatrixFile.finalSolution();
            } else if (!primalMatrixFile.hasBound()) {
                noBoundMessage();
            } else {
                while (!(primalMatrixFile.isMaximizeOptimal()) && primalMatrixFile.hasBound()) {

                    iteration++;

                    System.out.println("\n\n*********************************************");
                    System.out.println("**************** ITERATION " + iteration + " ****************");
                    System.out.println("*********************************************");

                    System.out.println("\nMaximum value: " + primalMatrixFile.findMaximumValue());
                    System.out.print("Maximum value's column index: " + primalMatrixFile.findMaximumValueColumnNumber());
                    System.out.println(" [" + primalMatrixFile.findMaximumValueVariable() + "]");
                    System.out.println("Minimum value: " + primalMatrixFile.findMinimumValue());
                    System.out.print("Minimum value's row index: " + primalMatrixFile.findMinimumValueRowNumber());
                    System.out.println(" [" + primalMatrixFile.findMinimumValueVariable() + "]\n");
                    primalMatrixFile.runSimplex();
                    primalMatrixFile.printMatrix("New Tableau");

                }

                if (primalMatrixFile.isMaximizeOptimal()) {
                    foundSolutionMessage();
                    primalMatrixFile.finalSolution();
                    System.out.println("\niterations: " + iteration);
                } else if (!primalMatrixFile.hasBound()) {
                    noBoundMessage();
                }
            }
            primalMatrixFile.writeToFile();

        } catch (Exception e) {
            System.out.println("Couldn't load matrix from file.");
        }


    }

}
