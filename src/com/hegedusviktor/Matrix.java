package com.hegedusviktor;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Matrix <Type extends Object> {

    private Type[][] matrix;
    private int rows;
    private int columns;
    private Scanner scanner = new Scanner(System.in);

    public Matrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    /**
     * This function generates a 2D array made of Object types
     * first row & column is made up of String types
     * others are made up of Integer/Double/Float types
     * @return
     */

    @SuppressWarnings("unchecked")
    public Type[][] generateTableau () {

        matrix = (Type[][]) new Object[this.rows][this.columns];
        int rowCounter = 1;
        int columnCounter = 1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                if (i == 0 && j != 0 && j != (matrix[0].length - 1)) {
                    String rowVariables = "x" + rowCounter++;
                    matrix[i][j] = (Type) new String(rowVariables);
                } else if (j == 0 && i != 0 && i != matrix.length - 1) {
                    String columnVariables = "y" + columnCounter++;
                    matrix[i][j] = (Type) new String(columnVariables);
                } else if (i == 0 && j == 0) {
                    matrix[i][j] = (Type) new String("  ");
                } else if (i == matrix.length - 1 && j == 0) {
                    matrix[i][j] = (Type) new String("Z ");
                } else if (i == 0 && j == matrix[0].length - 1) {
                    matrix[i][j] = (Type) new String("b");
                } else if (j != 0 && i != 0) {
                    Random rand = new Random();
                    int randomValue = rand.nextInt(20) + 1;
                    matrix[i][j] = (Type) new Double(randomValue);
                }
            }
        }
        return matrix;
    }

    /**
     * Prints the matrix to the console in the correct format
     * @param matrixName
     */

    @SuppressWarnings("unchecked")
    public void printMatrix (String matrixName) throws Exception {
        System.out.println("\n" + matrixName);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                /* assignment to the first row */
                if (i == 0 && j == matrix[i].length - 1) {
                    if (!(matrix[i][j] instanceof String)) {
                        throw new InvalidMatrixValues("Not valid type");
                    }
                    System.out.println("      " + matrix[i][j] + "  ");
                    for (int k = 0; k < matrix[i].length; k++) {
                        System.out.print("----------");
                    }

                /* assignment to the first column */
                } else if (i == 0 && j != 0 && j != matrix[0].length - 1) {
                    if (!(matrix[i][j] instanceof String)) {
                        throw new InvalidMatrixValues("\nNot valid type");
                    }
                    System.out.print(" " + matrix[i][j] + "     ");

                /* assignment to the first element */
                } else if (i == 0 && j == 0) {
                    if (!(matrix[i][j] instanceof String)) {
                        throw new InvalidMatrixValues("\nNot valid type");
                    }
                    System.out.print(matrix[i][j] + "   ");

                /* assignment to the last element of the first row */
                } else if (j == 0 && i != 0) {
                    if (!(matrix[i][j] instanceof String)) {
                        throw new InvalidMatrixValues("\nNot valid type");
                    }
                    System.out.print(matrix[i][j] + "   ");

                /* assignment to other elements (integers, double & float types) */
                } else if (j != 0 && i != 0) {
                    if (!(matrix[i][j] instanceof Double || matrix[i][j] instanceof Float ||
                            matrix[i][j] instanceof Integer)) {
                        throw new InvalidMatrixValues("Not valid type");
                    }
                    System.out.print(String.format("%.2f", matrix[i][j]) + "    ");
                }

                if (i == matrix.length - 2 && j == matrix[i].length - 1) {
                    System.out.println();
                    for (int l = 0; l < matrix[i].length; l++) {
                        System.out.print("----------");
                    }
                }

                if (i > 0 && j == matrix[i].length - 2) {
                    System.out.print("<=   ");
                }


            }
            System.out.println();
        }
    }


    /**
     * This method changes the value of the element
     * This method has three parameters.
     * The first two is the position of the element, the third one is the new value
     * @param rowIndex
     * @param columnIndex
     * @param newVal
     */


    @SuppressWarnings("unchecked")
    public boolean changeValue (int rowIndex, int columnIndex, Type newVal) {

        boolean changed = false;
        if (rowIndex > matrix.length - 1 || columnIndex > matrix[0].length - 1 || rowIndex < 0 || columnIndex < 0) {
            System.out.println("Illegal parameters, please try again!");
        } else {

            addValueToElement:
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (rowIndex == 0 || columnIndex == 0) {
                        if (newVal instanceof String) {
                            matrix[rowIndex][columnIndex] = (Type) new String((String) newVal);
                            changed = true;
                            break addValueToElement;
                        } else {
                            System.out.println("Error, invalid value!\nEnter a string!");
                            break addValueToElement;
                        }
                    } else {
                        if (newVal instanceof Float) {
                            matrix[rowIndex][columnIndex] = (Type) new Double((Double) newVal);
                            changed = true;
                            break addValueToElement;
                        } else if (newVal instanceof  Double) {
                            matrix[rowIndex][columnIndex] = (Type) new Double((Double) newVal);
                            changed = true;
                        } else if (newVal instanceof  Integer) {
                            Double convertedValue = ((Integer) newVal).doubleValue();
                            matrix[rowIndex][columnIndex] = (Type) new Double(convertedValue);
                            changed = true;
                        } else {
                            System.out.println("\nError type: NaN -> only float, double and integer types are accepted");
                            System.out.println("Correct format for the new value (example): 10f or 10.4 or 10d or 10");
                            System.out.println("Enter a valid number!");
                            break addValueToElement;
                        }
                    }
                }
            }
        }
        return changed;
    }

    /**
     * Creates matrix from input values / user input is needed
     * @return
     */

    @SuppressWarnings("unchecked")
    public Type[][] createMatrixFromInput () {

        System.out.println("Enter matrix values: ");
        matrix = (Type[][]) new Object[this.rows][this.columns];
        int rowCounter = 1;
        int columnCounter = 1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                if (i == 0 && j != 0 && j != (matrix[0].length - 1)) {
                    String rowVariables = "x" + rowCounter++;
                    matrix[i][j] = (Type) new String(rowVariables);
                } else if (j == 0 && i != 0 && i != matrix.length - 1) {
                    String columnVariables = "y" + columnCounter++;
                    matrix[i][j] = (Type) new String(columnVariables);
                } else if (i == 0 && j == 0) {
                    matrix[i][j] = (Type) new String("  ");
                } else if (i == matrix.length - 1 && j == 0) {
                    matrix[i][j] = (Type) new String("Z ");
                } else if (i == 0 && j == matrix[0].length - 1) {
                    matrix[i][j] = (Type) new String("b");
                } else if (j != 0 && i != 0) {
                    matrix[i][j] = (Type) new Double(scanner.nextDouble());
                }
            }
        }
        return matrix;
    }

    /**
     * Creates matrix from input values / user input is needed
     * @return
     */

    @SuppressWarnings("unchecked")
    public Type[][] createMatrixFromFile () throws IOException {

        try {
            double[][] readMatrix = readMatrixFromFile();
            matrix = (Type[][]) new Object[this.rows][this.columns];
            int rowCounter = 1;
            int columnCounter = 1;

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {

                    if (i == 0 && j != 0 && j != (matrix[0].length - 1)) {
                        String rowVariables = "x" + rowCounter++;
                        matrix[i][j] = (Type) new String(rowVariables);
                    } else if (j == 0 && i != 0 && i != matrix.length - 1) {
                        String columnVariables = "y" + columnCounter++;
                        matrix[i][j] = (Type) new String(columnVariables);
                    } else if (i == 0 && j == 0) {
                        matrix[i][j] = (Type) new String("  ");
                    } else if (i == matrix.length - 1 && j == 0) {
                        matrix[i][j] = (Type) new String("Z ");
                    } else if (i == 0 && j == matrix[0].length - 1) {
                        matrix[i][j] = (Type) new String("b");
                    } else if (j != 0 && i != 0) {
                        matrix[i][j] = (Type) new Double(readMatrix[i - 1][j - 1]);
                    }
                }
            }
            return matrix;
        } catch (IOException e)  {
            throw new IOException("Error occured. Couldn't create simplex tableau.");
        }
    }

    /**
     * reads 2d array from file (inputMatrix.txt)
     * @return the 2d array itself
     */

    private double[][] readMatrixFromFile () throws IOException {

        String fileName = "inputMatrix.txt";
        double[][] readMatrix = new double[numberOfRows()][numberOfColumns()];
        int i = 0, j = 0;

        try (BufferedReader bufferedReader = new BufferedReader (new FileReader (new File(fileName)))){
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(" ");
                for (String str : values) {
                    try {
                        double value = Double.parseDouble(str);
                        readMatrix[i][j] = value;
                        j++;
                    } catch (NumberFormatException e) {
                        System.out.println();
                        throw new NumberFormatException("\nError type: NaN\nMatrix element is not a number.");
                    }
                }
                i++;
                j = 0;
            }
            return readMatrix;

        } catch (FileNotFoundException e){
            throw new FileNotFoundException("File (" + fileName + ") does not exist.");
        } catch (IOException e){
            throw new IOException("Error occured: " + e.getMessage());
        }
    }

    /**
     * number of columns in the matrix, which is in the inputMatrix.txt file
     * @return number of columns
     */

    public static int numberOfColumns () throws IOException {
        String fileName = "inputMatrix.txt";
        int maxColumn = Integer.MIN_VALUE;
        int j = 0;

        try (BufferedReader bufferedReader = new BufferedReader (new FileReader (new File (fileName)))){
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(" ");
                for (String str : values)  {
                    j++;
                    if (j > maxColumn) {
                        maxColumn = j;
                    }
                }
                j = 0;
            }
            return maxColumn;

        } catch (FileNotFoundException e){
            throw new FileNotFoundException("File (" + fileName + ") does not exist.");
        } catch (IOException e){
            throw new IOException("Error occured: " + e.getMessage());
        }
    }

    /**
     * number of rows in the matrix, which is in the inputMatrix.txt file
     * @return the number of rows
     */

    public static int numberOfRows () throws IOException {
        String fileName = "inputMatrix.txt";
        int row = 0;

        try (BufferedReader bufferedReader = new BufferedReader (new FileReader(new File (fileName)))){
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                row++;
            }
            return row;

        } catch (FileNotFoundException e){
            throw new FileNotFoundException("File (" + fileName + ") does not exist.");
        } catch (IOException e){
            throw new IOException("Error occured: " + e.getMessage());
        }
    }

    /**
     * Getter functions below
     * @return
     */

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Type[][] getMatrix() {
        return matrix;
    }
}