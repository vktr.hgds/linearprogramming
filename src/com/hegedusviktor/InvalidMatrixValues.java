package com.hegedusviktor;

public class InvalidMatrixValues extends Exception {
    public InvalidMatrixValues() {
        System.out.println("Error occured. Invalid matrix values have been found.");
    }

    public InvalidMatrixValues(String message) {
        super(message);
    }
}
